#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>

#include <mastodonpp/mastodonpp.hpp>

#include <unistd.h>

namespace
{
constexpr const char* GETOPT_OPTSTRING{"s:t:u:"};

std::string server{""};
std::string token{""};
std::string target_user{""};
std::string visibility{"unlisted"};

void parse_opts(int argc, char** argv);
void print_help();
} // namespace

int main(int argc, char** argv)
{
	try {
		parse_opts(argc, argv);
	}
	catch (std::runtime_error&) {
		print_help();
		return 1;
	}

	std::ostringstream message_buf;
	std::string message_line_buf;

	if (target_user.length() > 0)
		message_buf << target_user << ' ';

	while (!std::cin.eof()) {
		std::getline(std::cin, message_line_buf);
		message_buf << message_line_buf << '\n';
	}

	mastodonpp::Instance masto_instance(server.c_str(), token.c_str());
	mastodonpp::Connection masto_conn(masto_instance);

	std::string message = message_buf.str();

	mastodonpp::parametermap parameter_map{{"status", message},
	                                       {"visibility", visibility}};

	auto answer = masto_conn.post(mastodonpp::API::v1::statuses, parameter_map);
	std::cout << answer << std::endl;

	return 0;
}

namespace
{
void parse_opts(int argc, char** argv)
{
	int opt;
	while ((opt = getopt(argc, argv, GETOPT_OPTSTRING)) >= 0) {
		switch (opt) {
		case 's':
			server = optarg;
			break;
		case 't':
			token = optarg;
			break;
		case 'u':
			target_user = optarg;
			visibility = "direct";
			break;
		default:
			break;
		}
	}

	if (server.empty() || token.empty()) {
		throw std::runtime_error("Missing required arguments!");
	}
}

void print_help()
{
	std::cerr << "Usage: sendmastodon -s SERVER -t TOKEN -u USER\n";
}
} // namespace
